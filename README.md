# 青云控制台命令行接口

Java Doc项目文档：[点击此链接](http://accjiyun.gitee.io/qingcloudconsole/doc/)

## 介绍

基于 QingCloud API 实现 Command Line Interface ，即命令行接口。
包括：
 - 创建主机(RunInstances)
 - 获取主机(DescribeInstances)
 - 销毁主机(TerminateInstances)
 
 ## 配置
 
 使用前必需一个配置文件`config.properties`，配置你自己的 qy_access_key_id 和 qy_secret_access_key 以及 zone 。比如:
 
 ```properties
 qy_access_key_id: 'QINGCLOUDACCESSKEYID'
 qy_secret_access_key: 'QINGCLOUDSECRETACCESSKEYEXAMPLE'
 zone: 'pek1'
```

 access key 可在 [青云控制台](https://console.qingcloud.com/access_keys/)申请。zone 是你的资源所在的节点，可在控制台切换节点的地方查看，如 pek1, pek2, gd1 等。
 
 ## 输入参数
 
 命令语法可参考青云官方文档 [qingcloud-cli](https://docs.qingcloud.com/api/common/cli/index.html)，命令去掉头部`qingcloud iaas`, 其它基本一直。参数只有 int 和 string 类型。比如：
 
```
#创建主机
run-instances -m xenial3x64 -t c1m1 -l passwd -p passWD123456

#获取主机
describe-instances

#销毁主机
terminate-instances -i i-xnim88ub
```

### 支持的命令

```
usage: 
  <action> [parameters]

Here are valid actions:

  describe-instances
  run-instances
  terminate-instances
  help

optional arguments:
  -h, --help  show this help message and exit
```

### describe-instances

```
usage: DescribeInstancesExec
 -h,--help                    show this help message and exit
 -i,--instances.1 <arg>       the comma separated IDs of instances you
                              want to describe.
 -L,--limit <arg>             specify the number of the returning results.
 -m,--image_id.1 <arg>        the image id of instances.
 -O,--offset <arg>            the starting offset of the returning
                              results.
 -s,--status.1 <arg>          instance status: pending, running, stopped,
                              suspended, terminated, ceased
 -t,--instance_type.1 <arg>   instance type: small_b, small_c, medium_a,
                              medium_b, medium_c, large_a, large_b,
                              large_c
 -T,--tags.1 <arg>            the comma separated IDs of tags.
 -V,--verbose <arg>           the number to specify the verbose level,
                              larger the number, the more detailed
                              information will be returned.
 -W,--search_word <arg>       the combined search column
 -z,--zone <arg>              the ID of zone you want to access, this will
                              override zone ID in config file.
```


### run-instances

```
usage: RunInstancesExec
 -C,--cpu <arg>                           cpu core: 1, 2, 4, 8, 16
 -c,--count <arg>                         the number of instances to
                                          launch, default 1.
 -h,--help                                show this help message and exit
 -hostname,--hostname <arg>               the hostname you want to specify
                                          for the new instance.
 -i,--instance_class <arg>                instance class: 0 is
                                          performance; 1 is high
                                          performance, default 0.
 -k,--login_keypair <arg>                 login_keypair, should specified
                                          when SSH login mode is
                                          "keypair".
 -l,--login_mode <arg>                    SSH login mode: keypair or
                                          passwd
 -m,--image_id <arg>                      image ID
 -M,--memory <arg>                        memory size in MB: 512, 1024,
                                          2048, 4096, 8192, 16384
 -N,--instance_name <arg>                 instance name
 -n,--vxnets <arg>                        specifies the IDs of vxnets the
                                          instance will join.
 -need_userdata,--need_userdata <arg>     use userdata
 -p,--login_passwd <arg>                  login_passwd, should specified
                                          when SSH login mode is "passwd".
 -s,--security_group <arg>                the ID of security group that
                                          will be applied to instance
 -t,--instance_type <arg>                 instance type: small_b, small_c,
                                          medium_a, medium_b,medium_c,
                                          large_a, large_b, large_c
 -target_user,--target_user <arg>         ID of user who will own this
                                          resource, should be one of your
                                          sub-account.
 -userdata_path,--userdata_path <arg>     userdata_path
 -userdata_type,--userdata_type <arg>     userdata_type: plain, exec, tar
 -userdata_value,--userdata_value <arg>   userdata_value
 -z,--zone <arg>                          the ID of zone you want to
                                          access, this will override zone
                                          ID in config file.
```

### terminate-instances

```
usage: TerminateInstancesExec
 -h,--help                show this help message and exit
 -i,--instances.1 <arg>   the comma separated IDs of instances you want to
                          terminate.
 -z,--zone <arg>          the ID of zone you want to access, this will
                          override zone ID in config file.
```

## 添加支持命令

 - 在`cn.accjiyun.cli.cmd`包中添加命令类，继承`CommandExec`，使用`@CLI`注解，添加命令参数和注解；
 - 添加对应命令名称常量（`CMDNAME`）；
 - API指令常量（`QingCloudAction`）；
 - 重写`exec()`中调用`qingCloudRequest.runAction(ACTIONCODE, paramMap)`执行命令动作，发送请求即可。

比如销毁主机命令类：

```java
@CLI({
        @Option(opt = "h", longOpt = "help", description = "show this help message and exit"),
        @Option(opt = "i", longOpt = "instances.1", description = "the comma separated IDs of instances you want to terminate.", required = true, hasArg = true),
        @Option(opt = "z", longOpt = "zone", description = "the ID of zone you want to access, this will override zone ID in config file.", hasArg = true, type = String.class, defaultValue = "pek3a")
})
public class TerminateInstancesExec extends CommandExec {
    /**
     * 命令名称
     */
    final String CMDNAME = "terminate-instances";
    /**
     * 命令描述
     */
    final String CMDDESCRIPTION = "terminate-instances -i \"instance_id,...\"";
    /**
     * API指令
     */
    final String ACTIONCODE = QingCloudAction.TERMINATE_INSTANCES;

    public TerminateInstancesExec() {
        thisClass = this.getClass();
    }

    /**
     * 执行命令动作
     * @param cmdHelper 命令行工具类
     */
    @Override
    public void exec(CommandLineHelper cmdHelper) {
        CommandLine cmd = cmdHelper.getCommandLine();
        Map<String, String> paramMap = new HashMap<>();
        optionToParamsMap(cmd.getOptions(), paramMap);
        QingCloudRequestImpl qingCloudRequest = new QingCloudRequestImpl();
        String response = qingCloudRequest.runAction(ACTIONCODE, paramMap);
        System.out.println(response);
    }

    /**
     * 获取命令名称
     * @return
     */
    @Override
    public String getCMDNAME() {
        return CMDNAME;
    }

}
```