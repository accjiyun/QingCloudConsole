package cn.accjiyun;

import cn.accjiyun.entity.constants.QingCloudAction;
import cn.accjiyun.qingcloud.QingCloudRequestImpl;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jiyun on 2017/11/4.
 */
public class InstancesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(InstancesTest.class);
    private QingCloudRequestImpl qingCloudRequest;
    private JsonObject jsonObject;

    @Before
    public void setUp() {
        qingCloudRequest = new QingCloudRequestImpl();
    }

    /**
     * 创建主机
     */
    @Test
    public void testRunInstances() {
        Map<String, String> paramMap  = new HashMap<>();
        paramMap.put("image_id", "xenial3x64");
        paramMap.put("instance_type", "c1m1");
        paramMap.put("login_mode", "passwd");
        paramMap.put("login_passwd", "passWD123456");
        String response = qingCloudRequest.runAction(QingCloudAction.RUN_INSTANCES, paramMap);
        JsonParser parser = new JsonParser();
        jsonObject = parser.parse(response).getAsJsonObject();
        LOGGER.info(response);
    }

    /**
     * 获取主机
     */
    @Test
    public void testDescribeInstances() {
        Map<String, String> paramMap  = new HashMap<>();
        String response = qingCloudRequest.runAction(QingCloudAction.DESCRIBE_INSTANCES, paramMap);
        LOGGER.info(response);
    }

    /**
     * 销毁主机
     */
    @Test
    public void testTerminateInstances() {
        Map<String, String> paramMap  = new HashMap<>();
//        String instanceId = jsonObject.getAsJsonArray("instance_set").get(0).getAsJsonObject().get("instance_id").getAsString();
        paramMap.put("instances.1", "i-meqpwbif");
        String response = qingCloudRequest.runAction(QingCloudAction.TERMINATE_INSTANCES, paramMap);
        LOGGER.info(response);
    }

}
