package cn.accjiyun.entity.response;

/**
 * Created by jiyun on 2017/10/31.
 */
public class ErrorResponse {

    /**
     * message : PermissionDenied, instance [i-2aypaijz] is not running, can not be stopped
     * ret_code : 1400
     */

    private String message;
    private int ret_code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRet_code() {
        return ret_code;
    }

    public void setRet_code(int ret_code) {
        this.ret_code = ret_code;
    }

    @Override
    public String toString() {
        return "{\"message\":\"" + message + "\""
                + ",\"ret_code\":\"" + ret_code + "}";
    }
}
