package cn.accjiyun.entity.response;

/**
 * Created by jiyun on 2017/11/1.
 */
public class TerminateInstancesResponse {

    /**
     * action : TerminateInstancesResponse
     * job_id : j-ybnoeitr
     * ret_code : 0
     */

    private String action;
    private String job_id;
    private int ret_code;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public int getRet_code() {
        return ret_code;
    }

    public void setRet_code(int ret_code) {
        this.ret_code = ret_code;
    }
}
