package cn.accjiyun.entity.response;

import java.util.List;

/**
 * Created by jiyun on 2017/11/1.
 */
public class DescribeInstancesResponse {

    /**
     * action : DescribeInstancesResponse
     * instance_set : [{"vxnets":[],"memory_current":1024,"extra":{"features":4,"ivshmem":[],"gpu_pci_nums":"","cpu_max":0,"cpu_model":"","mem_max":0,"hypervisor":"kvm","gpu":0,"gpu_class":0,"nic_mqueue":0},"image":{"ui_type":"tui","processor_type":"64bit","platform":"linux","features_supported":{"set_keypair":1,"disk_hot_plug":1,"user_data":1,"set_pwd":1,"root_fs_rw_offline":1,"root_fs_rw_online":1,"nic_hot_plug":1,"join_multiple_managed_vxnets":0,"reset_fstab":1},"image_size":20,"image_name":"Ubuntu Server 16.04.3 LTS 64bit","image_id":"xenial3x64","os_family":"ubuntu","provider":"system","features":0},"graphics_passwd":"052STjsp6DqM6AEc38xRBuGjsglJkL1f","dns_aliases":[],"alarm_status":"","owner":"usr-MsTccsMR","vcpus_current":1,"instance_id":"i-c1qld7x6","sub_code":0,"graphics_protocol":"vnc","instance_class":0,"status_time":"2017-11-01T15:57:01Z","status":"ceased","description":null,"cpu_topology":"","tags":[],"transition_status":"","eips":[],"repl":"rpp-00000000","volume_ids":[],"lastest_snapshot_time":null,"instance_name":"Ubuntu","instance_type":"c1m1","create_time":"2017-11-01T13:49:38Z","volumes":[]}]
     * total_count : 1
     * ret_code : 0
     */

    private String action;
    private int total_count;
    private int ret_code;
    private List<InstanceSetBean> instance_set;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getRet_code() {
        return ret_code;
    }

    public void setRet_code(int ret_code) {
        this.ret_code = ret_code;
    }

    public List<InstanceSetBean> getInstance_set() {
        return instance_set;
    }

    public void setInstance_set(List<InstanceSetBean> instance_set) {
        this.instance_set = instance_set;
    }

    public static class InstanceSetBean {
        /**
         * vxnets : []
         * memory_current : 1024
         * extra : {"features":4,"ivshmem":[],"gpu_pci_nums":"","cpu_max":0,"cpu_model":"","mem_max":0,"hypervisor":"kvm","gpu":0,"gpu_class":0,"nic_mqueue":0}
         * image : {"ui_type":"tui","processor_type":"64bit","platform":"linux","features_supported":{"set_keypair":1,"disk_hot_plug":1,"user_data":1,"set_pwd":1,"root_fs_rw_offline":1,"root_fs_rw_online":1,"nic_hot_plug":1,"join_multiple_managed_vxnets":0,"reset_fstab":1},"image_size":20,"image_name":"Ubuntu Server 16.04.3 LTS 64bit","image_id":"xenial3x64","os_family":"ubuntu","provider":"system","features":0}
         * graphics_passwd : 052STjsp6DqM6AEc38xRBuGjsglJkL1f
         * dns_aliases : []
         * alarm_status :
         * owner : usr-MsTccsMR
         * vcpus_current : 1
         * instance_id : i-c1qld7x6
         * sub_code : 0
         * graphics_protocol : vnc
         * instance_class : 0
         * status_time : 2017-11-01T15:57:01Z
         * status : ceased
         * description : null
         * cpu_topology :
         * tags : []
         * transition_status :
         * eips : []
         * repl : rpp-00000000
         * volume_ids : []
         * lastest_snapshot_time : null
         * instance_name : Ubuntu
         * instance_type : c1m1
         * create_time : 2017-11-01T13:49:38Z
         * volumes : []
         */

        private int memory_current;
        private ExtraBean extra;
        private ImageBean image;
        private String graphics_passwd;
        private String alarm_status;
        private String owner;
        private int vcpus_current;
        private String instance_id;
        private int sub_code;
        private String graphics_protocol;
        private int instance_class;
        private String status_time;
        private String status;
        private Object description;
        private String cpu_topology;
        private String transition_status;
        private String repl;
        private Object lastest_snapshot_time;
        private String instance_name;
        private String instance_type;
        private String create_time;
        private List<?> vxnets;
        private List<?> dns_aliases;
        private List<?> tags;
        private List<?> eips;
        private List<?> volume_ids;
        private List<?> volumes;

        public int getMemory_current() {
            return memory_current;
        }

        public void setMemory_current(int memory_current) {
            this.memory_current = memory_current;
        }

        public ExtraBean getExtra() {
            return extra;
        }

        public void setExtra(ExtraBean extra) {
            this.extra = extra;
        }

        public ImageBean getImage() {
            return image;
        }

        public void setImage(ImageBean image) {
            this.image = image;
        }

        public String getGraphics_passwd() {
            return graphics_passwd;
        }

        public void setGraphics_passwd(String graphics_passwd) {
            this.graphics_passwd = graphics_passwd;
        }

        public String getAlarm_status() {
            return alarm_status;
        }

        public void setAlarm_status(String alarm_status) {
            this.alarm_status = alarm_status;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public int getVcpus_current() {
            return vcpus_current;
        }

        public void setVcpus_current(int vcpus_current) {
            this.vcpus_current = vcpus_current;
        }

        public String getInstance_id() {
            return instance_id;
        }

        public void setInstance_id(String instance_id) {
            this.instance_id = instance_id;
        }

        public int getSub_code() {
            return sub_code;
        }

        public void setSub_code(int sub_code) {
            this.sub_code = sub_code;
        }

        public String getGraphics_protocol() {
            return graphics_protocol;
        }

        public void setGraphics_protocol(String graphics_protocol) {
            this.graphics_protocol = graphics_protocol;
        }

        public int getInstance_class() {
            return instance_class;
        }

        public void setInstance_class(int instance_class) {
            this.instance_class = instance_class;
        }

        public String getStatus_time() {
            return status_time;
        }

        public void setStatus_time(String status_time) {
            this.status_time = status_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getDescription() {
            return description;
        }

        public void setDescription(Object description) {
            this.description = description;
        }

        public String getCpu_topology() {
            return cpu_topology;
        }

        public void setCpu_topology(String cpu_topology) {
            this.cpu_topology = cpu_topology;
        }

        public String getTransition_status() {
            return transition_status;
        }

        public void setTransition_status(String transition_status) {
            this.transition_status = transition_status;
        }

        public String getRepl() {
            return repl;
        }

        public void setRepl(String repl) {
            this.repl = repl;
        }

        public Object getLastest_snapshot_time() {
            return lastest_snapshot_time;
        }

        public void setLastest_snapshot_time(Object lastest_snapshot_time) {
            this.lastest_snapshot_time = lastest_snapshot_time;
        }

        public String getInstance_name() {
            return instance_name;
        }

        public void setInstance_name(String instance_name) {
            this.instance_name = instance_name;
        }

        public String getInstance_type() {
            return instance_type;
        }

        public void setInstance_type(String instance_type) {
            this.instance_type = instance_type;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public List<?> getVxnets() {
            return vxnets;
        }

        public void setVxnets(List<?> vxnets) {
            this.vxnets = vxnets;
        }

        public List<?> getDns_aliases() {
            return dns_aliases;
        }

        public void setDns_aliases(List<?> dns_aliases) {
            this.dns_aliases = dns_aliases;
        }

        public List<?> getTags() {
            return tags;
        }

        public void setTags(List<?> tags) {
            this.tags = tags;
        }

        public List<?> getEips() {
            return eips;
        }

        public void setEips(List<?> eips) {
            this.eips = eips;
        }

        public List<?> getVolume_ids() {
            return volume_ids;
        }

        public void setVolume_ids(List<?> volume_ids) {
            this.volume_ids = volume_ids;
        }

        public List<?> getVolumes() {
            return volumes;
        }

        public void setVolumes(List<?> volumes) {
            this.volumes = volumes;
        }

        public static class ExtraBean {
            /**
             * features : 4
             * ivshmem : []
             * gpu_pci_nums :
             * cpu_max : 0
             * cpu_model :
             * mem_max : 0
             * hypervisor : kvm
             * gpu : 0
             * gpu_class : 0
             * nic_mqueue : 0
             */

            private int features;
            private String gpu_pci_nums;
            private int cpu_max;
            private String cpu_model;
            private int mem_max;
            private String hypervisor;
            private int gpu;
            private int gpu_class;
            private int nic_mqueue;
            private List<?> ivshmem;

            public int getFeatures() {
                return features;
            }

            public void setFeatures(int features) {
                this.features = features;
            }

            public String getGpu_pci_nums() {
                return gpu_pci_nums;
            }

            public void setGpu_pci_nums(String gpu_pci_nums) {
                this.gpu_pci_nums = gpu_pci_nums;
            }

            public int getCpu_max() {
                return cpu_max;
            }

            public void setCpu_max(int cpu_max) {
                this.cpu_max = cpu_max;
            }

            public String getCpu_model() {
                return cpu_model;
            }

            public void setCpu_model(String cpu_model) {
                this.cpu_model = cpu_model;
            }

            public int getMem_max() {
                return mem_max;
            }

            public void setMem_max(int mem_max) {
                this.mem_max = mem_max;
            }

            public String getHypervisor() {
                return hypervisor;
            }

            public void setHypervisor(String hypervisor) {
                this.hypervisor = hypervisor;
            }

            public int getGpu() {
                return gpu;
            }

            public void setGpu(int gpu) {
                this.gpu = gpu;
            }

            public int getGpu_class() {
                return gpu_class;
            }

            public void setGpu_class(int gpu_class) {
                this.gpu_class = gpu_class;
            }

            public int getNic_mqueue() {
                return nic_mqueue;
            }

            public void setNic_mqueue(int nic_mqueue) {
                this.nic_mqueue = nic_mqueue;
            }

            public List<?> getIvshmem() {
                return ivshmem;
            }

            public void setIvshmem(List<?> ivshmem) {
                this.ivshmem = ivshmem;
            }
        }

        public static class ImageBean {
            /**
             * ui_type : tui
             * processor_type : 64bit
             * platform : linux
             * features_supported : {"set_keypair":1,"disk_hot_plug":1,"user_data":1,"set_pwd":1,"root_fs_rw_offline":1,"root_fs_rw_online":1,"nic_hot_plug":1,"join_multiple_managed_vxnets":0,"reset_fstab":1}
             * image_size : 20
             * image_name : Ubuntu Server 16.04.3 LTS 64bit
             * image_id : xenial3x64
             * os_family : ubuntu
             * provider : system
             * features : 0
             */

            private String ui_type;
            private String processor_type;
            private String platform;
            private FeaturesSupportedBean features_supported;
            private int image_size;
            private String image_name;
            private String image_id;
            private String os_family;
            private String provider;
            private int features;

            public String getUi_type() {
                return ui_type;
            }

            public void setUi_type(String ui_type) {
                this.ui_type = ui_type;
            }

            public String getProcessor_type() {
                return processor_type;
            }

            public void setProcessor_type(String processor_type) {
                this.processor_type = processor_type;
            }

            public String getPlatform() {
                return platform;
            }

            public void setPlatform(String platform) {
                this.platform = platform;
            }

            public FeaturesSupportedBean getFeatures_supported() {
                return features_supported;
            }

            public void setFeatures_supported(FeaturesSupportedBean features_supported) {
                this.features_supported = features_supported;
            }

            public int getImage_size() {
                return image_size;
            }

            public void setImage_size(int image_size) {
                this.image_size = image_size;
            }

            public String getImage_name() {
                return image_name;
            }

            public void setImage_name(String image_name) {
                this.image_name = image_name;
            }

            public String getImage_id() {
                return image_id;
            }

            public void setImage_id(String image_id) {
                this.image_id = image_id;
            }

            public String getOs_family() {
                return os_family;
            }

            public void setOs_family(String os_family) {
                this.os_family = os_family;
            }

            public String getProvider() {
                return provider;
            }

            public void setProvider(String provider) {
                this.provider = provider;
            }

            public int getFeatures() {
                return features;
            }

            public void setFeatures(int features) {
                this.features = features;
            }

            public static class FeaturesSupportedBean {
                /**
                 * set_keypair : 1
                 * disk_hot_plug : 1
                 * user_data : 1
                 * set_pwd : 1
                 * root_fs_rw_offline : 1
                 * root_fs_rw_online : 1
                 * nic_hot_plug : 1
                 * join_multiple_managed_vxnets : 0
                 * reset_fstab : 1
                 */

                private int set_keypair;
                private int disk_hot_plug;
                private int user_data;
                private int set_pwd;
                private int root_fs_rw_offline;
                private int root_fs_rw_online;
                private int nic_hot_plug;
                private int join_multiple_managed_vxnets;
                private int reset_fstab;

                public int getSet_keypair() {
                    return set_keypair;
                }

                public void setSet_keypair(int set_keypair) {
                    this.set_keypair = set_keypair;
                }

                public int getDisk_hot_plug() {
                    return disk_hot_plug;
                }

                public void setDisk_hot_plug(int disk_hot_plug) {
                    this.disk_hot_plug = disk_hot_plug;
                }

                public int getUser_data() {
                    return user_data;
                }

                public void setUser_data(int user_data) {
                    this.user_data = user_data;
                }

                public int getSet_pwd() {
                    return set_pwd;
                }

                public void setSet_pwd(int set_pwd) {
                    this.set_pwd = set_pwd;
                }

                public int getRoot_fs_rw_offline() {
                    return root_fs_rw_offline;
                }

                public void setRoot_fs_rw_offline(int root_fs_rw_offline) {
                    this.root_fs_rw_offline = root_fs_rw_offline;
                }

                public int getRoot_fs_rw_online() {
                    return root_fs_rw_online;
                }

                public void setRoot_fs_rw_online(int root_fs_rw_online) {
                    this.root_fs_rw_online = root_fs_rw_online;
                }

                public int getNic_hot_plug() {
                    return nic_hot_plug;
                }

                public void setNic_hot_plug(int nic_hot_plug) {
                    this.nic_hot_plug = nic_hot_plug;
                }

                public int getJoin_multiple_managed_vxnets() {
                    return join_multiple_managed_vxnets;
                }

                public void setJoin_multiple_managed_vxnets(int join_multiple_managed_vxnets) {
                    this.join_multiple_managed_vxnets = join_multiple_managed_vxnets;
                }

                public int getReset_fstab() {
                    return reset_fstab;
                }

                public void setReset_fstab(int reset_fstab) {
                    this.reset_fstab = reset_fstab;
                }
            }
        }
    }
}
