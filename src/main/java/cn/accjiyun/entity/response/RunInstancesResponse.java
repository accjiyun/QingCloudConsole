package cn.accjiyun.entity.response;

import java.util.List;

/**
 * Created by jiyun on 2017/11/1.
 */
public class RunInstancesResponse {

    /**
     * action : RunInstancesResponse
     * instances : ["i-rtyv0968"]
     * job_id : j-bm6ym3r8
     * ret_code : 0
     */

    private String action;
    private String job_id;
    private int ret_code;
    private List<String> instances;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public int getRet_code() {
        return ret_code;
    }

    public void setRet_code(int ret_code) {
        this.ret_code = ret_code;
    }

    public List<String> getInstances() {
        return instances;
    }

    public void setInstances(List<String> instances) {
        this.instances = instances;
    }
}
