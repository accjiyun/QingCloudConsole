package cn.accjiyun.entity.constants;

/**
 * 执行命令
 * Created by jiyun on 2017/11/3.
 */
public class QingCloudAction {
    /**
     * 获取主机
     */
    public static String DESCRIBE_INSTANCES = "DescribeInstances";
    /**
     * 创建主机
     */
    public static String RUN_INSTANCES = "RunInstances";
    /**
     * 销毁主机
     */
    public static String TERMINATE_INSTANCES = "TerminateInstances";

}
