package cn.accjiyun;

import cn.accjiyun.cli.cmd.*;
import cn.accjiyun.cli.common.CommandLineHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by jiyun on 2017/10/31.
 */
public class ClientStart {

    /**
     * 支持的命令列表集合
     */
    private static Map<String, CommandExec> cmdMap = new HashMap<>();

    /**
     * 程序入口
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        initServer();
        NoParamExec.printHomeHelp();
        while (true) {
            String[] cmds = in.nextLine().trim().split("\\s+");
            CommandExec commandExec;
            CommandLineHelper cmdHelper = null;
            if (cmds == null || cmds.length == 0) {
                continue;
            } else {
                String cmdHand = cmds[0];
                if (!cmdMap.containsKey(cmdHand)) {
                    System.out.println("\"" + cmdHand + "\" not exist！");
                    continue;
                }
                commandExec = cmdMap.get(cmdHand);
                String[] params = new String[cmds.length - 1];
                System.arraycopy(cmds, 1, params, 0, params.length);
                try {
                    cmdHelper = new CommandLineHelper(commandExec.getThisClass());
                    if (cmdHelper.parse(params)) {
                        commandExec.exec(cmdHelper);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 初始化命令列表集合
     */
    public static void initServer() {

        CommandExec describeInstancesExec = new DescribeInstancesExec();
        cmdMap.put(describeInstancesExec.getCMDNAME(), describeInstancesExec);

        CommandExec runInstancesExec = new RunInstancesExec();
        cmdMap.put(runInstancesExec.getCMDNAME(), runInstancesExec);

        CommandExec terminateInstancesExec = new TerminateInstancesExec();
        cmdMap.put(terminateInstancesExec.getCMDNAME(), terminateInstancesExec);

        CommandExec noParamExec = new NoParamExec();
        cmdMap.put(noParamExec.getCMDNAME(), noParamExec);
    }
}
