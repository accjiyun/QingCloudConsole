package cn.accjiyun.cli.cmd;

import cn.accjiyun.cli.common.CommandLineHelper;
import org.apache.commons.cli.Option;

import java.util.Map;

/**
 * Created by jiyun on 2017/11/2.
 */
public abstract class CommandExec {
    /**
     * 命令名称
     */
    String CMDNAME;
    /**
     * 命令描述
     */
    String CMDDESCRIPTION;
    /**
     * API指令
     */
    String ACTIONCODE;
    /**
     * 子类
     */
    Class thisClass;
    /**
     * 命令行工具类
     */
    CommandLineHelper cmdHelper;

    /**
     * 执行命令动作
     * @param cmdHelper 命令行工具类
     */
    public abstract void exec(CommandLineHelper cmdHelper);

    /**
     * 获取命令名称
     * @return
     */
    public abstract String getCMDNAME();

    /**
     * 获取子类
     * @return 子类
     */
    public Class getThisClass() {
        return thisClass;
    }

    /**
     * 将Options 选项参数转换到Map键值对中
     * @param options 参数选项
     * @param paramMap 参数键值对
     */
    public static void optionToParamsMap(Option[] options, Map<String, String> paramMap) {
        for(Option option : options) {
            paramMap.put(option.getLongOpt(), option.getValue());
        }
    }
}
