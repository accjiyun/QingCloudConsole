package cn.accjiyun.cli.cmd;

import cn.accjiyun.cli.common.CLI;
import cn.accjiyun.cli.common.CommandLineHelper;
import cn.accjiyun.cli.common.Option;
import cn.accjiyun.common.utils.ParamUtil;
import cn.accjiyun.entity.constants.QingCloudAction;
import cn.accjiyun.entity.response.DescribeInstancesResponse;
import cn.accjiyun.qingcloud.QingCloudRequestImpl;
import org.apache.commons.cli.CommandLine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by jiyun on 2017/11/2.
 */
@CLI({
        @Option(opt = "h", longOpt = "help", description = "show this help message and exit"),
        @Option(opt = "i", longOpt = "instances.1", description = "the comma separated IDs of instances you want to describe.", hasArg = true),
        @Option(opt = "m", longOpt = "image_id.1", description = "the image id of instances.", hasArg = true),
        @Option(opt = "t", longOpt = "instance_type.1", description = "instance type: small_b, small_c, medium_a, medium_b, medium_c, large_a, large_b, large_c", hasArg = true),
        @Option(opt = "s", longOpt = "status.1", description = "instance status: pending, running, stopped, suspended, terminated, ceased", hasArg = true),
        @Option(opt = "W", longOpt = "search_word", description = "the combined search column", hasArg = true),
        @Option(opt = "T", longOpt = "tags.1", description = "the comma separated IDs of tags.", hasArg = true),
        @Option(opt = "V", longOpt = "verbose", description = "the number to specify the verbose level, larger the number, the more detailed information will be returned.", hasArg = true, type = Integer.class, defaultValue = "0"),
        @Option(opt = "O", longOpt = "offset", description = "the starting offset of the returning results.", hasArg = true, type = Integer.class, defaultValue = "0"),
        @Option(opt = "L", longOpt = "limit", description = "specify the number of the returning results.", hasArg = true, type = Integer.class, defaultValue = "20"),
        @Option(opt = "z", longOpt = "zone", description = "the ID of zone you want to access, this will override zone ID in config file.", hasArg = true, type = String.class, defaultValue = "pek3a")
})
public class DescribeInstancesExec extends CommandExec{
    /**
     * 命令名称
     */
    final String CMDNAME = "describe-instances";
    /**
     * 命令描述
     */
    final String CMDDESCRIPTION = "describe-instances [-i \"instance_id, ...\"] [options]";
    /**
     * API指令
     */
    final String ACTIONCODE = QingCloudAction.DESCRIBE_INSTANCES;

    public DescribeInstancesExec() {
        thisClass = this.getClass();
    }

    /**
     * 执行命令动作
     * @param cmdHelper 命令行工具类
     */
    @Override
    public void exec(CommandLineHelper cmdHelper) {
        CommandLine cmd = cmdHelper.getCommandLine();
        Map<String, String> paramMap = new HashMap<>();
        optionToParamsMap(cmd.getOptions(), paramMap);
        QingCloudRequestImpl qingCloudRequest = new QingCloudRequestImpl();
        String response = qingCloudRequest.runAction(ACTIONCODE, paramMap);
        System.out.println(response);
    }

    /**
     * 获取命令名称
     * @return
     */
    @Override
    public String getCMDNAME() {
        return CMDNAME;
    }

}
