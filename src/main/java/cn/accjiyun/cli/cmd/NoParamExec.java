package cn.accjiyun.cli.cmd;

import cn.accjiyun.cli.common.CLI;
import cn.accjiyun.cli.common.CommandLineHelper;
import cn.accjiyun.cli.common.Option;

import java.util.Map;

/**
 * Created by jiyun on 2017/11/2.
 */
@CLI({
        @Option(opt = "h", longOpt = "help", description = "show this help message and exit"),
})
public class NoParamExec extends CommandExec{
    /**
     * 命令名称
     */
    final String CMDNAME = "help";

    public NoParamExec() {
        thisClass = this.getClass();
    }

    /**
     * 执行命令动作
     * @param cmdHelper 命令行工具类
     */
    @Override
    public void exec(CommandLineHelper cmdHelper) {
        printHomeHelp();
    }

    public static void printHomeHelp() {
        String helpMessage = "usage: \n" +
                "  <action> [parameters]\n" +
                "\n" +
                "Here are valid actions:\n" +
                "\n" +
                "  describe-instances\n" +
                "  run-instances\n" +
                "  terminate-instances\n" +
                "  help\n" +
                "\n" +
                "optional arguments:\n" +
                "  -h, --help  show this help message and exit";
        System.out.println(helpMessage);
    }

    /**
     * 获取命令名称
     * @return
     */
    @Override
    public String getCMDNAME() {
        return CMDNAME;
    }
}
