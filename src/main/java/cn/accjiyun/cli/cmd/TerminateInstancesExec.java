package cn.accjiyun.cli.cmd;

import cn.accjiyun.cli.common.CLI;
import cn.accjiyun.cli.common.CommandLineHelper;
import cn.accjiyun.cli.common.Option;
import cn.accjiyun.entity.constants.QingCloudAction;
import cn.accjiyun.qingcloud.QingCloudRequestImpl;
import org.apache.commons.cli.CommandLine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jiyun on 2017/11/2.
 */
@CLI({
        @Option(opt = "h", longOpt = "help", description = "show this help message and exit"),
        @Option(opt = "i", longOpt = "instances.1", description = "the comma separated IDs of instances you want to terminate.", required = true, hasArg = true),
        @Option(opt = "z", longOpt = "zone", description = "the ID of zone you want to access, this will override zone ID in config file.", hasArg = true, type = String.class, defaultValue = "pek3a")
})
public class TerminateInstancesExec extends CommandExec {
    /**
     * 命令名称
     */
    final String CMDNAME = "terminate-instances";
    /**
     * 命令描述
     */
    final String CMDDESCRIPTION = "terminate-instances -i \"instance_id,...\"";
    /**
     * API指令
     */
    final String ACTIONCODE = QingCloudAction.TERMINATE_INSTANCES;

    public TerminateInstancesExec() {
        thisClass = this.getClass();
    }

    /**
     * 执行命令动作
     * @param cmdHelper 命令行工具类
     */
    @Override
    public void exec(CommandLineHelper cmdHelper) {
        CommandLine cmd = cmdHelper.getCommandLine();
        Map<String, String> paramMap = new HashMap<>();
        optionToParamsMap(cmd.getOptions(), paramMap);
        QingCloudRequestImpl qingCloudRequest = new QingCloudRequestImpl();
        String response = qingCloudRequest.runAction(ACTIONCODE, paramMap);
        System.out.println(response);
    }

    /**
     * 获取命令名称
     * @return
     */
    @Override
    public String getCMDNAME() {
        return CMDNAME;
    }

}
