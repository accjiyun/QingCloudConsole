package cn.accjiyun.cli.cmd;

import cn.accjiyun.cli.common.CLI;
import cn.accjiyun.cli.common.CommandLineHelper;
import cn.accjiyun.cli.common.Option;
import cn.accjiyun.entity.constants.QingCloudAction;
import cn.accjiyun.qingcloud.QingCloudRequestImpl;
import org.apache.commons.cli.CommandLine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jiyun on 2017/11/2.
 */
@CLI({
        @Option(opt = "h", longOpt = "help", description = "show this help message and exit"),
        @Option(opt = "m", longOpt = "image_id", description = "image ID", required = true, hasArg = true),
        @Option(opt = "t", longOpt = "instance_type", description = "instance type: small_b, small_c, medium_a, medium_b,medium_c, large_a, large_b, large_c", hasArg = true),
        @Option(opt = "C", longOpt = "cpu", description = "cpu core: 1, 2, 4, 8, 16", hasArg = true, type = Integer.class, defaultValue = "1"),
        @Option(opt = "M", longOpt = "memory", description = "memory size in MB: 512, 1024, 2048, 4096, 8192, 16384", hasArg = true, type = Integer.class, defaultValue = "1024"),
        @Option(opt = "c", longOpt = "count", description = "the number of instances to launch, default 1.", hasArg = true, type = Integer.class, defaultValue = "1"),
        @Option(opt = "N", longOpt = "instance_name", description = "instance name", hasArg = true),
        @Option(opt = "l", longOpt = "login_mode", description = "SSH login mode: keypair or passwd", required = true, hasArg = true, type = String.class, defaultValue = "passwd"),
        @Option(opt = "k", longOpt = "login_keypair", description = "login_keypair, should specified when SSH login mode is \"keypair\".", hasArg = true),
        @Option(opt = "p", longOpt = "login_passwd", description = "login_passwd, should specified when SSH login mode is \"passwd\".", hasArg = true),
        @Option(opt = "n", longOpt = "vxnets", description = "specifies the IDs of vxnets the instance will join.", hasArg = true),
        @Option(opt = "s", longOpt = "security_group", description = "the ID of security group that will be applied to instance", hasArg = true),
        @Option(opt = "hostname", longOpt = "hostname", description = "the hostname you want to specify for the new instance.", hasArg = true),
        @Option(opt = "i", longOpt = "instance_class", description = "instance class: 0 is performance; 1 is high performance, default 0.", hasArg = true),
        @Option(opt = "need_userdata", longOpt = "need_userdata", description = "use userdata", hasArg = true, type = Integer.class, defaultValue = "0"),
        @Option(opt = "userdata_type", longOpt = "userdata_type", description = "userdata_type: plain, exec, tar", hasArg = true),
        @Option(opt = "userdata_value", longOpt = "userdata_value", description = "userdata_value", hasArg = true),
        @Option(opt = "userdata_path", longOpt = "userdata_path", description = "userdata_path", hasArg = true, defaultValue = "/etc/qingcloud/userdata"),
        @Option(opt = "target_user", longOpt = "target_user", description = "ID of user who will own this resource, should be one of your sub-account.", hasArg = true),
        @Option(opt = "z", longOpt = "zone", description = "the ID of zone you want to access, this will override zone ID in config file.", hasArg = true, type = String.class, defaultValue = "pek3a")
})
public class RunInstancesExec extends CommandExec {
    /**
     * 命令名称
     */
    final String CMDNAME = "run-instances";
    /**
     * 命令描述
     */
    final String CMDDESCRIPTION = "run-instances --image_id <image_id> --instance_type <instance_type> [options] [-f <conf_file>]";
    /**
     * API指令
     */
    final String ACTIONCODE = QingCloudAction.RUN_INSTANCES;

    public RunInstancesExec() {
        thisClass = this.getClass();
    }

    /**
     * 执行命令动作
     * @param cmdHelper 命令行工具类
     */
    @Override
    public void exec(CommandLineHelper cmdHelper) {
        CommandLine cmd = cmdHelper.getCommandLine();
        Map<String, String> paramMap = new HashMap<>();
        optionToParamsMap(cmd.getOptions(), paramMap);
        QingCloudRequestImpl qingCloudRequest = new QingCloudRequestImpl();
        String response = qingCloudRequest.runAction(ACTIONCODE, paramMap);
        System.out.println(response);
    }

    /**
     * 获取命令名称
     * @return
     */
    @Override
    public String getCMDNAME() {
        return CMDNAME;
    }
}
