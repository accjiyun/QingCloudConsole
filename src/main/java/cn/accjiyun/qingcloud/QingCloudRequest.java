package cn.accjiyun.qingcloud;

import cn.accjiyun.entity.response.DescribeInstancesResponse;
import cn.accjiyun.entity.response.RunInstancesResponse;
import cn.accjiyun.entity.response.TerminateInstancesResponse;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

/**
 * Created by jiyun on 2017/11/3.
 */
public interface QingCloudRequest {
    /**
     * 执行命令
     * @param actionCode API指令
     * @param paramMap 参数对
     * @return
     */
    String runAction(String actionCode, Map<String, String> paramMap);

    /**
     * 发送请求
     * @param urlStr
     * @return
     */
    String sendRequest(String urlStr);

}
