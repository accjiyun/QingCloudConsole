package cn.accjiyun.qingcloud;

import cn.accjiyun.common.utils.PropertyUtil;

/**
 * Created by jiyun on 2017/11/2.
 */
public class QingCloudConstants {
    /**
     * 配置文件名
     */
    public static final String propertyFile = "config";
    /**
     * 配置文件属性工具类
     */
    public static final PropertyUtil propertyUtil = PropertyUtil.getInstance(propertyFile);
    /**
     * 请求方式
     */
    public static final String requestMethod = "GET";
    /**
     * API 入口
     */
    public static final String ENDPOINT = "https://api.qingcloud.com/iaas/";
    /**
     * 	区域 ID
     */
    public static final String ZONE = propertyUtil.getProperty("zone");
    /**
     * 时间戳格式
     */
    public static final String TIME_STAMP = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    /**
     * Qingcloud 密钥ID
     */
    public static final String ACCESS_KEY_ID = propertyUtil.getProperty("qy_access_key_id");
    /**
     * 	API的版本号
     */
    public static final String VERSION = "1";
    /**
     * 签名所用哈希算法
     */
    public static final String SIGNATURE_METHOD = "HmacSHA256";
    /**
     * 签名函数的版本号
     */
    public static final String SIGNATURE_VERSION = "1";
    /**
     * Qingcloud 密钥
     */
    public static final String QY_SECRET_ACCESS_KEY = propertyUtil.getProperty("qy_secret_access_key");


}
